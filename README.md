# Tugas 2
## Detail tugas dan app ini

Buatlah proyek Toko Online dengan ketentuan berikut:

- Memiliki halaman login dengan proses autentikasi sederhana
- Memiliki halaman utama dengan navigasi ke minimal 3 halaman
- Memanfaatkan components, state, lifecycle, penanganan events, fragment

Terdapat kombinasi penggunaan class component tanpa hook dan function
component dengan hooks (`useState` dan `useEffect`)

#### Author
Muh Aswar BAKRI - DTS UI FE 02 /Kelompok 7
Gottfried CPN - DTS UI FE 02 /Kelompok 7