import React from 'react'
import { NavLink } from 'react-router-dom'

export default function NavBar() {
    return (
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand" href="#">Toko-u</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" 
            data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown"
            aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavDropdown">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item">
                        <NavLink className="nav-link" exact to="/">Beli</NavLink>
                    </li>
                    <li class="nav-item">
                        <NavLink className="nav-link" exact to="/about">About</NavLink>
                    </li>
                </ul>
                <NavLink className="btn btn-primary" exact to="/login">Login</NavLink>
            </div>
        </nav>
    )
}