import React from 'react';
import logo from './logo.svg';
import './App.css';
import NavBar from './components/NavBar'
import LoginPage from './pages/LoginPage'
import BeliPage from './pages/BeliPage'
import AboutPage from './pages/AboutPage'
import { Route } from 'react-router-dom';

function App() {
  return (
    // contoh Fragmen
    <React.Fragment>
      <NavBar />
      <Route exact path="/" component={BeliPage} />
      <Route exact path="/login" component={LoginPage} />
      <Route exact path="/about" component={AboutPage} />
    </React.Fragment>
  );
}

export default App;

// backup dari App() untuk jaga-jaga
function Original() {
  return (
    <header className="App-header">
      <img src={logo} className="App-logo" alt="logo" />
      <p>
        Edit <code>src/App.js</code> and save to reload.
        </p>
      <a
        className="App-link"
        href="https://reactjs.org"
        target="_blank"
        rel="noopener noreferrer"
      >
        Learn React
        </a>
    </header>
  )
}